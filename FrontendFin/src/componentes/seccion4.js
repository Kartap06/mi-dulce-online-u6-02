import React from "react";
import Footer from "../componentes/Footer";
import imgLogo from "../images/LogoF.png"
import "../css/seccion4.css";

const Seccion4 = () => {
  return (
    <div className="container-div">
      <div className="container-seccion4">
        <Footer img={imgLogo}/>
      </div>
    </div>
  );
};

export default Seccion4;
