import React from 'react';
import iconAdmin from "../images/admin.png";


const btnAdmin = () => {
  return (
    <div className="container">
      <img src={iconAdmin} alt="" width="50" height="40"/>
    </div>
  )
};

export default btnAdmin
