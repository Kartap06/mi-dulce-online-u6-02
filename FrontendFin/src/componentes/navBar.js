import React from "react";
import { Link, useNavigate } from "react-router-dom";
import LogoF from "../images/LogoF.png";
import Button from "./buttons";
import BtnExit from "./btnExit";
import Cart from "./cart";
import Admin from "./btnAdmin";
import "../css/navBar.css";

const NavBar = () => {
  const navigate = useNavigate();

  const cerrarSesion = () => {
    localStorage.removeItem("token");
    navigate("/");
  };

  return (
    <div className="navBar">
      <div className="Container-Fluid">
        <Link className="img" to={"/Home"}>
          <img src={LogoF} alt="logo" width="140" height="140" />
        </Link>
        <ul className="navBar-nav">
          <li className="nav-item">
            <Button texto="CHOCOLATE" />
          </li>
          <li className="nav-item">
            <Button texto="DULCES" />
          </li>
          <li className="nav-item">
            <Button texto="GALLETAS" />
          </li>
          <li className="nav-item">
            <Button texto="GOMAS" />
          </li>
          <li className="nav-item">
            <Button texto="SNACKS" />
          </li>
          <li className="nav-item">
            <Link to={""} className="nav-link">
              <Cart />
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/productos-admin"} className="nav-link">
              <Admin/>
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/"} className="nav-link">
              <strong onClick={cerrarSesion}>
                <BtnExit />
              </strong>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default NavBar;
