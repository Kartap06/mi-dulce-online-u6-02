import React, {useState,useEffect} from "react";
import APIInvoke from "../utils/APIInvoke";
import Card from "../componentes/card";
import "../css/seccion3.css";

const Seccion3 = () => {

  const [productos, setProductos] = useState([]);

  const cargarProductos = async () => {
    const response = await APIInvoke.invokeGET("/api/productos");    
    setProductos(response);
  };

  useEffect(() => {
    cargarProductos();
  },[]);
  
  return (
    <div className="container-seccion3">
      {productos.map((item) => (
        <Card key={item._id}
          image={item.imagen} 
          nombre={item.nombre} 
          precio={item.precio_venta} 
          categoria={item.categoria}
        />
      ))}
    </div>
  );
};

export default Seccion3;
