import React from "react";
import ImageSlider from "./Slider";
import { SliderData } from '../componentes/SliderData';
import "../css/seccion2.css";

const Seccion2 = () => {
  return (
    <div>
      <ImageSlider slides={SliderData} />       
    </div>
  );
};

export default Seccion2;