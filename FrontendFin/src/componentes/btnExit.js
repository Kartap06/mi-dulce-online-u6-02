import React from "react";
import iconExit from "../images/exit.png";
import "../css/btnExit.css"

const BtnExit = ({cerrar}) => {
  return (
    <div className="container" onClick={cerrar}>
      <img src={iconExit} alt="" width="50" height="40"/>
    </div> 
  );
};

export default BtnExit;
