import React from "react";
import "../css/card.css";

const card = ({ image, nombre, precio, categoria }) => {  

  return (
    <div className="card" style={{ width: "18rem" }}>
      <img src={image} className="card-img-top" alt="ImgProducto"/>
      <div className="card-body">
        <h5 className="card-title"><b>{nombre}</b></h5>
        <br />
        <hr />
        <h6 className="card-categoria">
          {" "}
          <b>{categoria}</b>
        </h6>
        <h6 className="card-precio">          
          ${precio}
        </h6>
        <button to={""} className="btn btn-primary">
          Agregar al carrito
        </button>
      </div>
    </div>
  );
};

export default card;
