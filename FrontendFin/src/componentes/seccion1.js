import React from "react";
import NavBar from "./navBar";
import Seccion2 from "./seccion2";
import Seccion3 from "./seccion3";
import Seccion4 from "./seccion4";

const Seccion = () => {
  return (
    <div className="container-seccion1">
      <NavBar />
      <Seccion2/>    
      <Seccion3/>
      <Seccion4/>
    </div>
  );
};

export default Seccion;
