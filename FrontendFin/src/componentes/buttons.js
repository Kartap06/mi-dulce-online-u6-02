import React from "react";
import "../css/buttons.css";

function Button({ texto, presionar }) {
  return (
    <button className="Button" onClick={presionar}>
      {texto}
    </button>
  );
}

export default Button;