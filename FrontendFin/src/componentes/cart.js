import React from "react";
import iconCar from "../images/carrito.png"; 

const Cart = () => {

  return (
    <div className="container" >
      <img src={iconCar} alt="" width="50" height="40"/>
    </div>
  );
};

export default Cart;
