import React from "react";
import { Link } from "react-router-dom";
import logo from "../images/LogoF.png";
import "../css/footer.css";

const Footer = ({img}) => {
  return (
    <div>
      <footer className="pie-pagina">
        <div className="grupo-1">
          <div className="box">
            <figure>
              <Link href="#">
                <img src={logo} alt="Logo de SLee D" />
              </Link>
            </figure>
          </div>
          <div className="box">
            <h2>SOBRE NOSOTROS</h2>
            <p>Lo mejor en chocolates y dulceria a nivel nacional</p>
            <p>Nosotros somos distribuidores autorizados </p>
            <p>Tienda principal Cra. 8a #21-41 Bogotá, Colombia</p>
            <p>MiDulceOnline@gmail.com</p>
            <p>+57 (321) 444 7521 - +57 (311) 245 0114</p>
          </div>
          <div className="box">
            <h2>SIGUENOS EN NUESTRAS REDES SOCIALES</h2>
            <div className="red-social">
              <Link href="#" className="fa fa-facebook" />
              <Link href="#" className="fa fa-instagram" />
              <Link href="#" className="fa fa-twitter" />
              <Link href="#" className="fa fa-youtube" />
              <Link href="#" className="fa fa-whatsapp" />
            </div>
          </div>
        </div>
        <div className="grupo-2">
          <small>
            © 2022 <b>Mi Dulce Online</b> - Todos los Derechos Reservados.
          </small>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
