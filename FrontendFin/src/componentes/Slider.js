import React, { useState } from "react";
import { SliderData } from './SliderData';
import arrowRight from "../images/arrowRight.png";
import iconLeft from "../images/arrowLeft.png";


import "../css/Slider.css";


const Slider = ({ slides }) => {

  const [current, setCurrent] = useState(0);
  const length = slides.length;

  
  const nextSlide = () => {
    setCurrent(current === length - 1 ? 0 : current + 1);
  };

  const prevSlide = () => {
    setCurrent(current === 0 ? length - 1 : current - 1);
  };

  if (!Array.isArray(slides) || slides.length <= 0) {
    return null;
  }

  return (
    <section className='slider'>    
      
      <img src={arrowRight} alt="" className='right-arrow' onClick={prevSlide} width="50"/>
      <img src={iconLeft} alt="" className='left-arrow' onClick={nextSlide} width="50"/>
      
      {SliderData.map((slide, index) => {
        return (
          <div
            className={index === current ? 'slide active' : 'slide'}
            key={index}
          >
            {index === current && (
              <img src={slide.image} alt='travelimage' className='image' />
            )}
          </div>
        );
      })}
    </section>
    
  );
};

export default Slider;
