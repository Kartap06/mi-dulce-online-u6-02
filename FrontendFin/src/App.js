import React, {Fragment} from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Login from "./paginas/auth/Login";
import CrearCuenta from "./paginas/auth/CrearCuenta";
import ProductosAdmin from "./paginas/productos/productosAdmin";
import ProductosCrear from "./paginas/productos/productosCrear";
import ProductosEditar from "./paginas/productos/productosEditar";
import Seccion1 from "./componentes/seccion1";
import './App.css';


function App() {
  return (    
    <Fragment>
    <Router>
      <Routes>
        <Route path='/' exact element={<Login />} />     
        <Route path='/Home' exact element={<Seccion1/>} /> 
        <Route path='/crear-cuenta' exact element={<CrearCuenta />} />
        <Route path='/productos-admin' exact element={<ProductosAdmin />} />        
        <Route path='/productos-crear' exact element={<ProductosCrear />} />
        <Route path='/productos-editar/:idproducto' exact element={<ProductosEditar />} />
      </Routes>               
    </Router>    
  </Fragment>    
	);		
}
export default App;