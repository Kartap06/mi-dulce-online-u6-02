const mongoose = require("mongoose");

const clienteSchema = mongoose.Schema({
  nombre: { 
    type: String,
    required: true,
    trim: true 
  },
  apellido: { 
    type: String, 
    equired: true, 
    trim: true 
  },
  email: 
  { type: String, 
    required: true, 
    trim: true, 
    unique: true 
  },
  direccion: { 
    type: String, 
    required: true, 
    trim: true 
  },
  telefono: { 
    type: String, 
    required: true, 
    trim: true 
  },
  password: { 
    type: String, 
    required: true, 
    trim: true 
  },
});

module.exports = mongoose.model("Cliente", clienteSchema);