const express = require('express');
const conectarDB = require('./config/db');
const cors = require ('cors');


const app = express();
const PORT = process.env.PORT || 3000;

// conectar DB
conectarDB();
app.use(cors());
app.use(express.json());
app.use('/api/productos', require('./routes/producto'));
app.use('/api/clientes', require('./routes/cliente'));
app.use('/api/auth', require('./routes/auth'));


//muestra mensaje en el navegador 
//Rutas
app.get("/", (req,res) => {
    res.send("Bienvenidos esta configurado su servidor");
});

//Añadir para mi servidor escuche por un puerto
app.listen(PORT,() => console.log('El servidor esta conectado',PORT));