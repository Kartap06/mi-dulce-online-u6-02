const Cliente = require("../models/Cliente");
const bcryptjs = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");

exports.crearCliente = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }

  const { email, password } = req.body;
  try {
    //Verificamos si el usuario ya existe
    let cliente = await Cliente.findOne({ email });
    if (cliente) {
      return res.status(400).json({ msg: "El usuario ya existe" });
    }
    // creamos nuestro Usuario
    cliente = new Cliente(req.body);
    Cliente.password = await bcryptjs.hash(password, 10);
    await cliente.save();
    res.send(cliente);

    //Firmar el JWT
    const payload = {
      usuario: { id: usuario.id },
    };

    jwt.sign(
      payload,
      process.env.SECRETA,
      {
        expiresIn: 3600, //1 hora
      },
      (error, token) => {
        if (error) throw error;
        //Mensaje de confirmación
        res.json({ token });
      }
    );
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

exports.mostrarClientes = async (req, res) => {
  try {
    const cliente = await Cliente.find();
    res.json(cliente);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

exports.obtenerCliente = async (req, res) => {
  try {
    let cliente = await Cliente.findById(req.params.id);
    if (!cliente) {
      res.status(404).json({ msg: "el Usuario no existe" });
    }
    res.json(cliente);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

exports.eliminarCliente = async (req, res) => {
  try {
    let cliente = await Cliente.findById(req.params.id);
    if (!cliente) {
      res.status(404).json({ msg: "el Usuario no existe" });
    }
    await Cliente.findByIdAndRemove({ _id: req.params.id });
    res.json({ msg: "Usuario eliminado con exito" });
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

exports.actualizarCliente = async (req, res) => {
  try {
    const { nombre, apellido, email, direccion, telefono, password } = req.body;
    let cliente = await Cliente.findById(req.params.id);
    if (!usuario) {
      res.status(404).json({ msg: "el Usuario no existe" });
    }
    Cliente.nombre = nombre;
    Cliente.apellido = apellido;
    Cliente.email = email;
    Cliente.direccion = direccion;
    Cliente.telefono = telefono;
    Cliente.password = password;

    cliente = await Cliente.findOneAndUpdate({ _id: req.params.id }, cliente, {
      new: true,
    });
    res.json(cliente);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};
