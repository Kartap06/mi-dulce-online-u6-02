// Armamos las Rutas para  nuestro Usuario

const express = require('express');
const router = express.Router();
const clienteController = require('../controllers/clienteController');
const { check } = require("express-validator");



// rutas CRUD

router.get('/', clienteController.mostrarClientes);
router.post('/', clienteController.crearCliente);
router.get('/:id', clienteController.obtenerCliente);
router.put('/:id', clienteController.actualizarCliente);
router.delete('/:id', clienteController.eliminarCliente);

module.exports = router;

