# Mision TIC  2022 Grupo U6-02

![UIS](./Imagenes/MisionTIC-UIS.png)

# Integrantes
1. Deiby Pinilla:Desarrollador Backend 
2. Karen Tapias: Desarrollador Backend
3. Gustavo Dominguez:Desarrollador Frontend
4. Yohan Vela:Desarrollador Frontend
5. Christian Jimenez:Desarrollador Backend

# Componenentes
1. **Backend**: Este repositorio
2. **Frontend**: Este repositorio

# Proyecto Seleccionado

**MiDulceOnline**
![MiDulceOnline](./Imagenes/LogoF.png)

### Objetivos
 1.Diseñar una aplicación que actualice, consulte, inserte y elimine los diferentes productos del inventario de mercancías.

 2.Diseñar una aplicación que ayude a gestionar los clientes del negocio permitiendo crear nuevos clientes, editar, consultar y borrar los clientes existentes.

 3.Diseñar una aplicación amigable para el cliente y que sea responsive. 

 ### GitLab
**Ver proyecto** [Aquí](https://gitlab.com/Kartap06/mi-dulce-online-u6-02)

### Jira

[Jira](https://kartapias.atlassian.net/jira/software/projects/U0/boards/4/backlog).

### Heroku Frontend

[HerokuF](https://frontend-dulceonline.herokuapp.com/)

### Heroku Backend

[HerokuB](https://frontend-dulceonline.herokuapp.com/)

### Vista Final

![Login](./Imagenes/Login.jpg)

![Home](./Imagenes/Home.jpg)

![Productos](./Imagenes/Productos.jpg)









